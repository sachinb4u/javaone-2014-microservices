package weatherservice.qos.impl;

import java.util.concurrent.CopyOnWriteArrayList;

import weatherservice.qos.QualityOfService;

public class QualityOfServiceCommand {
	private CopyOnWriteArrayList<QualityOfService> m_services = new CopyOnWriteArrayList<>();
	
	public void average() {
		for (QualityOfService qos : m_services) {
			System.out.println("[" + qos.serviceId() + "] " + qos.serviceName() + ": " + qos.averageInvocationTime() + " ms.");
		}
	}

	public void add(QualityOfService service) {
		m_services.add(service);
	}

	public void remove(QualityOfService service) {
		m_services.remove(service);
	}
}

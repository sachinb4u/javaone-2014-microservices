package weatherservice.node;

public interface Node {
	String name();
	boolean healthCheck();
}
